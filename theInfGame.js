// DOM
const gameDiv = document.getElementById("game")
const scoreDiv = document.getElementById("score")
const marteau = document.getElementById("marteau")
const strikeZone = document.querySelector('.zone-test')
const endDiv = document.getElementById('end')
const homeAnim = document.getElementById("home-anim")

// MESURES & POSITIONS FOR GAMEDIV
const X = gameDiv.offsetLeft
const Y = gameDiv.offsetTop
const W = gameDiv.offsetWidth
const H = gameDiv.offsetHeight

const START = document.getElementById('start')
const STOP = document.getElementById('stop')


// AUDIO SOUNDS
const sounds = ['hammer2.wav', 'hammer3.wav', 'hammer4.wav']
const menuSong = new Audio("music/menu.mp3")
const gameSong = new Audio("music/game.mp3")
const gameOverSound = new Audio("sounds/gameover.wav")
let audioCtx;


// TIMER FOR MONSTER FREQUENCY APPEARANCE
const timeout = 500
// GAMEOVER CONDITION
const MONSTER_MAX = 30
let mobs_array = []

let GAME // setinterval function
let ISPLAYING = false // Bool for click action

let score = 0
let counter = 1 // for monster Id

// ONLOADED
window.onload = () => {
    document.querySelector("#audio-accept > h2").textContent = "Cliquez pour accepter l'Audio !!"
    console.log("page is loaded.");
    // AUDIO ACCEPTATION
    document.getElementById("audio-accept").onclick = () => {

        // AUDIO CTX
        let AudioContext = window.AudioContext || window.webkitAudioContext;
        audioCtx = new AudioContext({
            latencyHint: 'interactive',
            sampleRate: 44100,
        });

        // DOM
        document.getElementById("audio-accept").classList.add("hidden")
        menuSong.currentTime = 0
        menuSong.loop = true
        menuSong.play()
    }
}





/**
 * Construction of a mob
 */
class Mob {
    /**
     * Constructor :
     * @param {DomElement} img Img element 
     * @param {Integer} life Number of strikes before diyng (strike 1 by 1 pts)
     * @param {Integer} points Points won if diyng.
     */
    constructor(img = null, life = 0, points = 0) {
        this.img = img
        this.life = life
        this.points = points
    }
}




// START
START.onclick = () => {

    // REINIT FOR STARTING GAME
    homeAnim.classList.toggle("hidden", true)
    endDiv.classList.toggle("hidden", true)
    START.classList.add("hidden")
    STOP.classList.remove("hidden")
    GAME = setInterval(running, timeout)
    score = 0
    scoreDiv.textContent = score
    counter = 1
    ISPLAYING = true

    // AUDIO
    menuSong.pause()
    menuSong.currentTime = 0
    gameSong.currentTime = 0
    gameSong.loop = true
    gameSong.play()
}

// STOP
STOP.onclick = () => {
    endGame()
}

// IMG MOUSE MVT FOLLOW
gameDiv.addEventListener("mousemove", (e) => {
    marteau.style.top = e.clientY - 90 + "px"
    marteau.style.left = e.clientX + 58 + "px"
    strikeZone.style.top = e.clientY - (strikeZone.offsetHeight / 2) + "px"
    strikeZone.style.left = e.clientX - (strikeZone.offsetWidth / 2) + "px"
})

// OUT OF SCREEN
gameDiv.addEventListener("onmouseout", (e) => {
    console.log("OUT");
    marteau.style.top = e.clientY - 50 + "px"
    marteau.style.left = e.clientX - 25 + "px"
})

// CLICK - HIT ACTION
gameDiv.addEventListener("click", () => {
    if (ISPLAYING) {

        // FOREACH MOB OBJECT IN MOBS_ARRAY
        for (let i = 0; i < mobs_array.length; i++) {

            // COLLISION FUNCTION
            if (strikeZone.offsetLeft > mobs_array[i].img.offsetLeft &&
                strikeZone.offsetLeft < mobs_array[i].img.offsetLeft + mobs_array[i].img.offsetWidth &&
                strikeZone.offsetTop > mobs_array[i].img.offsetTop &&
                strikeZone.offsetTop < mobs_array[i].img.offsetTop + mobs_array[i].img.offsetHeight) {

                // STTTRRRIIIKKKKKEEEE
                mobs_array[i].life--

                if (0 >= mobs_array[i].life) {

                    // AUDIO
                    let audio = new Audio("sounds/hammer1.wav")
                    audio.currentTime = 0
                    audio.volume = .8
                    audio.play()

                    // ANIMATION AT CLICK POSITION IF DIYING
                    let circle = document.getElementById("explosion")
                    circle.classList.add("exp-anim")
                    circle.style.top = strikeZone.offsetTop + "px"
                    circle.style.left = strikeZone.offsetLeft + "px"
                    setTimeout(() => {
                        circle.classList.remove("exp-anim")
                    }, 300);

                    // ADD SCORE
                    score += mobs_array[i].points
                    scoreDiv.textContent = score

                    // REMOVE FROM DOM
                    gameDiv.removeChild(mobs_array[i].img)

                    // REMOVE MOB OBJECT FROM ARRAY
                    mobs_array.splice(i, 1)
                }
            }
        }
    }
    else {
        // BEAUTIFULL LOG ;)
        let css = "font-weight:bold;font-size:1.25rem;color:dodgerblue;"
        let css2 = "font-weight:bold;font-size:1.75rem;color:orange;"
        console.log(
            '%cCLICK %cSTART', css, css2
        );
    }

    // IDX FOR AUDIO CHOICES
    let idx = Math.floor(Math.random() * sounds.length)

    // AUDIO
    let audio = new Audio("sounds/" + sounds[idx])
    console.log(sounds[idx]);
    audio.volume = .35
    audio.currentTime = 0
    audio.play()

    // MARTEAU ANIMATION
    marteau.classList.add("frappe")
    setTimeout(() => {
        marteau.classList.remove("frappe")
    }, 300);
})




// ---------------------------------------------------------------------------




/**
 * GAME IS RUNNING
 */
function running() {
    getMob()
    gameLogic()
}

/**
 * END OF THE GAME
 */
function endGame() {

    if (ISPLAYING) {
        // AUDIO
        gameSong.pause()
        gameSong.currentTime = 0
        gameOverSound.currentTime = 0
        gameOverSound.volume = .45
        gameOverSound.play()

        // WAIT GameOverSong ENDING
        setTimeout(() => {
            // AUDIO
            menuSong.currentTime = 0
            menuSong.loop = true
            menuSong.play()
            homeAnim.classList.toggle("hidden", false)
            STOP.classList.add("hidden")
            START.classList.remove("hidden")
        }, 5000)

        // STOP GAME AND RE-INIT THE VALUES
        clearInterval(GAME);
        ISPLAYING = false
        let imgs = document.querySelectorAll(".mobs")
        for (let i = 0; i < imgs.length; i++) { gameDiv.removeChild(imgs[i]) }
        endDiv.classList.remove("hidden")
        mobs_array = []
    } else {
        console.log("STOPPED");
    }
}

/**
 * GAME LOGIC
 */
function gameLogic() {
    // VERIFYING END GAME STATEMENT

    // if 10 mobs on the screen in the same time
    if (document.querySelectorAll(".mobs").length >= MONSTER_MAX) {
        endGame()
    }
}

/**
 * GET RANDOM TYPES MOB
 */
function getMob() {

    // GET RANDOM NUMBER 
    let i = Math.floor(Math.random() * 3) + 1

    // MAKE AN IMAGE FOR MOB
    let img = new Image()
    img.id = "mob_" + counter
    counter++
    img.className = "mobs mob" + i
    img.src = "images/mob" + i + ".png"
    img.style.animationDelay = (Math.random() * 1/3).toFixed(2) + "s"
    img.style.animationDuration = Math.random() * 3 + 1 + "s"

    // MAKE NEW MOB
    let mob = {}

    // LVL
    switch (i) {
        // DEFINE WITH POSITION AND CREATE A NEW MOB
        case 1:
            img.width = 38;
            img.style.left = Math.floor(Math.random() * ((X + W - img.width) - X + 1) + X) + "px"
            img.style.top = Math.floor(Math.random() * ((Y + H - img.width) - Y + 1) + Y) + "px"
            mob = new Mob(img, 1, 10)
            break;
        case 2:
            img.width = 51;
            img.style.left = Math.floor(Math.random() * ((X + W - img.width) - X + 1) + X) + "px"
            img.style.top = Math.floor(Math.random() * ((Y + H - img.width) - Y + 1) + Y) + "px"
            mob = new Mob(img, 2, 20)
            break;
        case 3:
            img.width = 92
            img.style.left = Math.floor(Math.random() * ((X + W - img.width) - X + 1) + X) + "px"
            img.style.top = Math.floor(Math.random() * ((Y + H - img.width) - Y + 1) + Y) + "px"
            mob = new Mob(img, 4, 50)
            break;
        default:
            console.warn("NO MONSTER NAMED ", img.src);
    }

    // IF MOB, PUSH IT (Array and Screen)
    if (mob) {
        mobs_array.push(mob)
        gameDiv.append(img)
    }
}